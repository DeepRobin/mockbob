import { Router, Request, Response } from "express";
import { text } from "body-parser";
import { Mockbob } from "../mockbob";

const router: Router = Router();
const mockbob = new Mockbob();

router.post("/", function(req, res) {
  let text: string = req.body.text;
  let response: any = {};
  if (text) {
    if (text.length <= 1000) {
      response["text"] = mockbob.generateMockedString(text);
      res.send(response);
    } else {
      res.status(400);
      response["error"] = "String 'text' is too big";
      res.send(response);
    }
  } else {
    res.status(400);
    response["error"] = "no information provided";
    res.send(response);
  }
});

export const ApiController: Router = router;
