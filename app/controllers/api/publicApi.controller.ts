import { Router, Request, Response } from "express";
import * as bodyParser from "body-parser";
import { Mockbob } from "../../mockbob";
import { Errors } from './errors';

const router: Router = Router();
const mockbob = new Mockbob();

router.post("/", function(req, res) {
  let text: string = req.body.text;
  let response: any = {};
  if (text) {
    if (text.length <= 1000) {
      response["text"] = mockbob.generateMockedString(text);
      res.send(response);
    } else {
      res.status(400);
      let error : any = {};
      error["code"] = Errors.TEXT_TO_BIG;
      error["message"] = "String 'text' is too big";
      response["error"] = error;
      res.send(response);
    }
  } else {
    res.status(400);
    let error : any = {};
      error["code"] = Errors.NO_INFORMATION_PROVIDED;
      error["message"] = "no information provided";
      response["error"] = error;
    res.send(response);
  }
});

export const PublicAPI: Router = router;
