export const Errors = {
    NO_INFORMATION_PROVIDED: 0x1,
    TEXT_TO_BIG: 0x2,
    // not implemented (todo):
    TOO_MUCH_REQUESTS: 0x4
}