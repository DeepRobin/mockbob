import { Router, Request, Response } from "express";
import fs from "fs";
import path from "path";
import marked from "marked";

const router: Router = Router();

let getSidebarEntries = function(
  prefix: string,
  thePath: string,
  callback: any
) {
  let entries: any[] = [];

  fs.readdir(thePath, function(err, files) {
    if (err) {
      console.log(err);
    } else {
      let lenght = files.length;
      files.forEach(function(value, index) {
        let thePath2 = path.join(thePath, value);
        fs.lstat(thePath2, function(err, stats) {
          if (err) {
            console.log(err);
          } else {
            if (stats.isDirectory()) {
              getSidebarEntries(
                path.basename(thePath2).toUpperCase() + ": ",
                thePath2,
                function(array: any[]) {
                  array.forEach(function(value) {
                    entries.push(value);
                  });
                }
              );
            } else if (stats.isFile()) {
              if (!prefix) {
                prefix = "";
              }
              let url = "";
              if (path.dirname(thePath2).toLowerCase() === "docs")
                url = "/docs/" + path.basename(thePath2);
              else
                url = path.join(
                  "/docs/",
                  path.basename(path.dirname(url)),
                  path.basename(thePath2)
                );
              let entry = {
                url: url,
                title: getTitle(prefix, thePath2)
              };
              console.log(entry);

              entries.push(entry);
            }
          }
        });

        if (index == lenght - 1) callback(entries);
      });
    }
  });
};

let getTitle = function(prefix: string, thePath: string): string {
  fs.readFile(thePath, function(err, data) {
    if (err) {
      console.log(err);
    } else {
      let text = data.toString("utf8");
      text.split("\n").forEach(function(value) {
        if (value.trimLeft().startsWith("#")) {
          return prefix + value.substring(1).trim();
        }
      });
    }
  });

  return prefix + "Unknown document";
};

router.get(/^\/(.+)/, function(req, res) {
  let thePath = path.join(__dirname, "../", "../", "docs", req.params[0]);

  fs.exists(thePath, function(exists) {
    if (exists) {
      fs.readFile(thePath, function(err, data) {
        if (err) {
          console.log(thePath);
          console.log(err);
        } else
          marked.parse(data.toString("utf8"), function(err, result) {
            if (err) {
              console.log(err);
            } else {
              getSidebarEntries(
                "",
                path.join(__dirname, "../", "../", "docs"),
                function(entries: any) {
                  console.log(entries);
                  res.render("docs/docpage", {
                    markdownSource: result,
                    sideBarEntries: entries
                  });
                }
              );
            }
          });
      });
    }
  });
});

export const DocController: Router = router;
