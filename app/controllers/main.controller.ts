import { Router, Request, Response } from "express";

const router: Router = Router();

router.get("/", function(req, res) {
  res.render("index", {});
});

export const MainController: Router = router;
