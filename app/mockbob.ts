export class Mockbob {
  
    public generateMockedString(text: string): string {
        let array = Array.from(text);
        for (let i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                array[i] = array[i].toUpperCase();
            }
        }

        return array.join("");
    }
}