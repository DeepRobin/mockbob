import path from "path";

import express from "express";
import * as bodyParser from "body-parser";

import { MainController, ApiController } from "./controllers";
import { PublicAPI } from './controllers/api/publicApi.controller';
import { DocController } from './controllers/doc.controller';

const app: express.Application = express();
const port: number = Number(process.env.PORT) || 3000;

app.use(bodyParser.text())
app.use(bodyParser.raw())
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

app.set("view engine", "pug");
app.use("/css/", express.static(path.join(__dirname, 'public', 'css')))
app.use("/", express.static(path.join(__dirname, '../', 'public')))
app.use("/", MainController);
app.use("/internalApi", ApiController);
app.use("/api", PublicAPI);
app.use("/docs/", DocController);

app.listen(port, () => {
  console.log(`Listening mockbob page at :${port}`);
});
