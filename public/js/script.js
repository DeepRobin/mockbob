"use strict";

let mockbob = {
  mockText(text, callback) {
    var http = new XMLHttpRequest();
    var url = "/internalApi/";
    var params = "text=" + encodeURIComponent(text);
    http.open("POST", url, true);

    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onreadystatechange = function() {
      if (http.readyState == 4 && http.status == 200) {
        let response = JSON.parse(http.responseText);
        callback(response["text"]);
      }
    };
    http.send(params);
  }
};

let onSubmit = function() {
  let sourceInput = document.getElementById("sourceText");
  let output = document.getElementById("outputText");

  mockbob.mockText(sourceInput.value, function(text) {
    output.value = text;
  });
};

document.addEventListener(
  "DOMContentLoaded",
  function() {
    document.getElementById("submitButton").addEventListener("click", onSubmit);

    if ("serviceWorker" in navigator) {
      navigator.serviceWorker.register("/js/sw.js").then(function() {
        console.log("Service Worker registered!");
      });
    }
  },
  false
);
