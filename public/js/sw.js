const assets = [
  "/",
  "/?homescreen=1",
  "/css/style.css",
  "/js/script.js",
  "/js/sw.js",
  "/img/background.png"
];

self.addEventListener("beforeinstallprompt", function(e) {
  e.prompt();
});

self.addEventListener("install", async event => {
  const cache = await caches.open("mockbob");
  cache.addAll(assets);
});

self.addEventListener("activate", event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener("fetch", event => {
  event.respondWith(fetchCache(event.request));
});

async function fetchCache(request) {
  const cachedResponse = await caches.match(request);
  return cachedResponse || fetch(request);
}
